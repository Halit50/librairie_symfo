<?php

namespace App\Controller;

use App\Repository\AuteurRepository;
use App\Repository\CategorieRepository;
use App\Repository\LivreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(AuteurRepository $auteurRepository, LivreRepository $livreRepository, CategorieRepository $categorieRepository): Response
    {
        $livres = $livreRepository->findAll();
        $auteurs = $auteurRepository->findAll();
        $categories = $categorieRepository->findAll();
        return $this->render('home/index.html.twig', [
            'livres' => $livres,
            'auteurs' => $auteurs,
            'categories' => $categories
        ]);
    }
}
