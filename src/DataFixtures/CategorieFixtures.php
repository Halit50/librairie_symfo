<?php

namespace App\DataFixtures;

use App\entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategorieFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $categorie = new Categorie;
        $categorie->setTitre('Roman');
        $categorie->setDescription('Lorem Ipsum');
        $manager->persist($categorie);
        $this->addReference('Roman', $categorie);

        $categorie = new Categorie;
        $categorie->setTitre('Policier');
        $categorie->setDescription('Les enquetes de Scherlock Holmes');
        $manager->persist($categorie);
        $this->addReference('Policier', $categorie);

        $categorie = new Categorie;
        $categorie->setTitre('Historique');
        $categorie->setDescription('Histoire du début à la fin');
        $manager->persist($categorie);
        $this->addReference('Historique', $categorie);

        $manager->flush();
    }
}
