<?php

namespace App\DataFixtures;

use App\Entity\Livre;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LivreFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $livre = new Livre();
        $livre->setImage('blake-mortimer-tome-26-vallee-des-immortels.jpg');
        $livre->setTitre('Vallee des immortels');
        $livre->setDescription('Livre bourrée d\'énigmes');
        $livre->setAuteur($this->getReference('Cussler'));
        $livre->setCategorie($this->getReference('Roman'));
        $manager->persist($livre);

        $livre = new Livre();
        $livre->setImage('blake-mortimer-tome-27-le-cri-du-moloch.jpg');
        $livre->setTitre('Le cri du Moloch');
        $livre->setDescription('Le livre remplis de cri');
        $livre->setAuteur($this->getReference('Roba'));
        $livre->setCategorie($this->getReference('Policier'));
        $manager->persist($livre);

        $livre = new Livre();
        $livre->setImage('blake-mortimer-tome-28-le-dernier-espadon-couv.jpg');
        $livre->setTitre('Le dernier espadon');
        $livre->setDescription('Le livre remplis qui parle du dernier espadon');
        $livre->setAuteur($this->getReference('Verdon'));
        $livre->setCategorie($this->getReference('Historique'));
        $manager->persist($livre);

        $manager->flush();
    }
}
