<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $hasher;

    public function __construct(UserPasswordHasherInterface $passHasher)
    {
        $this->hasher = $passHasher;
    }
    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setRoles(['ROLE_USER']);
        $user->setEmail('test@gmail.com');
        $plainPassword = "pass";
        $pass = $this->hasher->hashPassword($user, $plainPassword);
        $user->setPassword($pass);
        $manager->persist($user);

        $manager->flush();
    }
}
