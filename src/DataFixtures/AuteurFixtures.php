<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\Auteur;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AuteurFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $auteur = new Auteur();
        $auteur->setNom('Cussler');
        $auteur->setPrenom('Clive');
        $auteur->setNaissance(new DateTime('31-12-1930'));
        $auteur->setPhoto('clive-cussler.jpg');
        $auteur->setDescription('Lorem ipsum');
        $manager->persist($auteur);
        $this->addReference('Cussler', $auteur);

        $auteur = new Auteur();
        $auteur->setNom('Roba');
        $auteur->setPrenom('Jean');
        $auteur->setNaissance(new DateTime('31-12-1940'));
        $auteur->setPhoto('jean-roba.jpg');
        $auteur->setDescription('Lorem ipsum 2');
        $manager->persist($auteur);
        $this->addReference('Roba', $auteur);

        $auteur = new Auteur();
        $auteur->setNom('Verdon');
        $auteur->setPrenom('John');
        $auteur->setNaissance(new DateTime('31-12-1950'));
        $auteur->setPhoto('john-verdon.jpg');
        $auteur->setDescription('Lorem ipsum 3');
        $manager->persist($auteur);
        $this->addReference('Verdon', $auteur);

        $auteur = new Auteur();
        $auteur->setNom('Schulz');
        $auteur->setPrenom('Charles');
        $auteur->setNaissance(new DateTime('31-12-1960'));
        $auteur->setPhoto('schulz-charles.jpg');
        $auteur->setDescription('Lorem ipsum 4');
        $manager->persist($auteur);
        $this->addReference('Schulz', $auteur);

        $auteur = new Auteur();
        $auteur->setNom('Van Hamme');
        $auteur->setPrenom('Jean');
        $auteur->setNaissance(new DateTime('31-12-1970'));
        $auteur->setPhoto('van-hamme-jean.jpg');
        $auteur->setDescription('Lorem ipsum 5');
        $manager->persist($auteur);
        $this->addReference('Van Hamme', $auteur);

        $manager->flush();
    }
}
